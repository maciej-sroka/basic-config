#!/bin/bash

export DISPLAY=:0

BATTERY_LEVEL=`cat /sys/class/power_supply/BAT0/capacity`
BATTERY_STATUS=`cat /sys/class/power_supply/BAT0/status`



if [ $BATTERY_STATUS = 'Discharging' ]; then
  if [ $BATTERY_LEVEL -lt 20 ]; then
    echo 'too low battery level while discharging'
    DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus notify-send 'Battery level' 'battery level too low, connect charger'
    paplay /usr/share/sounds/ubuntu/notifications/Xylo.ogg --volume 100000
  fi
fi

echo $BATTERY_LEVEL
echo $BATTERY_STATUS
