#!/bin/bash

echo `pwd`
echo $USER

yes | sudo apt-get install zsh
curl -L git.io/antigen > ~/.config/antigen.zsh
sudo chsh -s `which zsh` $USER



ln -s "`pwd`/zshrc" ~/.zshrc
ln -s "`pwd`/gitconfig" ~/.gitconfig
ln -s "`pwd`/vimrc" ~/.vimrc
# ln -s "`pwd`/config.fish" ~/.config/fish/config.fish
