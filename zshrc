source "$HOME/.config/antigen.zsh"
antigen use oh-my-zsh

# FIX ALT+ARROW ON MAC
bindkey -e
bindkey '[C' forward-word
bindkey '[D' backward-word

# UTILS
short_pwd() {
      cwd=$(pwd | sed "s<^$HOME<~<"  | perl -F/ -ane 'print join( "/", map { $i++ < @F - 2 ?  substr $_,0,1 : $_ } @F)')
          echo -n $cwd
}


# PLUGINS
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle colored-man-pages
antigen bundle mollifier/cd-gitroot


# ALIASES
alias gitcd='cd-gitroot'
alias zc='vi ~/.zshrc'
alias zr='source ~/.zshrc'
alias spwd='echo `short_pwd`'
alias pbcopy="tr -d '\n' | clipcopy"


antigen apply

# PROMPT
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$fg[yellow]%}]%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$reset_color%}"
PROMPT='${ret_status} %{$fg[cyan]%}$(short_pwd)%{$reset_color%}%{$fg[yellow]%}$(git_prompt_info) '






export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

alias gnomeconf="env XDG_CURRENT_DESKTOP=GNOME gnome-control-center"

nm-applet &
indicator-sound-switcher &

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

alias prscn="import /tmp/screenshot.png; xclip -selection clipboard -t image/png -i /tmp/screenshot.png"
